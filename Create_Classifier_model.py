#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  2 11:32:44 2018

@author: nassereddine
"""

from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.preprocessing.image import ImageDataGenerator
import numpy as np
from keras.preprocessing import image
import numpy as np
import cv2
import os 
import glob
Classifier = Sequential()
Classifier.add(Conv2D(32, (3, 3), input_shape = (64, 64, 3), activation = 'relu'))
Classifier.add(MaxPooling2D(pool_size = (2, 2)))
Classifier.add(Flatten())
Classifier.add(Dense(units = 128, activation = 'relu'))
#initialise our output layer
Classifier.add(Dense(units = 1, activation = 'sigmoid'))
#compile the CNN model : 
#choose the stochastic gradient descent algorithm.
#Loss parameter is to choose the loss function
#the metrics parameter is to choose the performance metric
Classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])
#before we fit our images to the neural network,
# we need to perform some image augmentations on them,
# which is basically synthesising the training data. 
#We are going to do this using keras.preprocessing 
#library for doing the synthesising part as well as 
#to prepare the training set as well as the test test 
#set of images that are present in a properly structured directories,
# where the directory’s name is take as the label of all the images 
#present in it
#All the images inside the ‘cats’ named folder will be 
#considered as cats by keras.
train_datagen = ImageDataGenerator(rescale = 1./255,shear_range = 0.2,
zoom_range = 0.2,
horizontal_flip = True)
test_datagen = ImageDataGenerator(rescale = 1./255)
training_set = train_datagen.flow_from_directory('/home/nassereddine/Téléchargements/CNN_Data/training_set',
target_size = (64, 64),
batch_size = 32,
class_mode = 'binary')
test_set = test_datagen.flow_from_directory('/home/nassereddine/Téléchargements/CNN_Data/test_set',
target_size = (64, 64),
batch_size = 32,
class_mode = 'binary')
#Now lets fit the data to our model 
#In the above code, ‘steps_per_epoch’ holds
# the number of training images, i.e the number
# of images the training_set folder contains.
#And ‘epochs’, A single epoch is a single step 
#in training a neural network; in other words when 
#a neural network is trained on every training samples
# only in one pass we say that one epoch is finished. So 
#training process should consist more than one epochs.In 
#this case we have defined 25 epochs.


Classifier.fit_generator(training_set,
steps_per_epoch = 8000,
epochs = 5,
validation_data = test_set,
validation_steps = 2000)
#Save the deep leerning model 
from keras.models import load_model

Classifier.save('/home/nassereddine/python-workspace/HiveRec/Classifier_model.h5')  # creates a HDF5 file 'my_model.h5'
del Classifier  # deletes the existing model