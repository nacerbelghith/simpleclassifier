#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 24 22:28:31 2018

@author: nassereddine
"""

# Importing the Keras libraries and packages
from keras.preprocessing.image import ImageDataGenerator
import numpy as np
from keras.preprocessing import image
from keras.models import load_model
import matplotlib.pyplot as plt
# returns a compiled model
# identical to the previous one
FRmodel = load_model('/home/nassereddine/python-workspace/HiveRec/Classifier_model.h5')
#Making new predictions from our trained model :

test_image1 = image.load_img('/home/nassereddine/Téléchargements/1200px-Slender-horned_gazelle_(Cincinnati_Zoo).jpg', target_size = (64, 64))
test_image = image.img_to_array(test_image1)
test_image = np.expand_dims(test_image, axis = 0)
#print(test_image)
width, height = test_image1.size
print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")
print(width,height)
print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%")

result = FRmodel.predict(test_image)
train_datagen = ImageDataGenerator(rescale = 1./255,shear_range = 0.2,
zoom_range = 0.2,
horizontal_flip = True)
training_set = train_datagen.flow_from_directory('/home/nassereddine/Téléchargements/CNN_Data/training_set',
target_size = (64, 64),
batch_size = 32,
class_mode = 'binary')
if result[0][0] == 1:
 prediction = 'dog'
else:
 prediction = 'cat'
 #visualizer  will show the model in a pdf form
print(prediction)
print(result)
imgplot = plt.imshow(test_image1)
plt.show()


from ann_visualizer.visualize import ann_viz;
ann_viz(FRmodel, title="Classifier neural network")